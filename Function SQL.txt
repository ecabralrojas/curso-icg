USE [Gestion_Supermercados_2]
GO

/****** Object:  UserDefinedFunction [dbo].[ICGO_FN_COMPRASARTICULOS_SCALAR]    Script Date: 7/29/2016 11:08:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Elvis,,Cabral>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[ICGO_FN_COMPRASARTICULOS_SCALAR]
(	@P_FECHAINIT DATETIME, --Parametros de fecha de inicio del reportes
	@P_FECHAFIN DATETIME)
RETURNS @TBL_COMPRASARTICULOS TABLE
(REFERENCIA NVARCHAR(18),
 DESCRIPCION NVARCHAR(60),
 SERIDOC NVARCHAR(4),
 NUMERODOC INT,
 FECHADOC DATE,
 CANTIDAD FLOAT,
 COSTO FLOAT,
 ACUMULADOCOMPRAS FLOAT,
 FACTORMONEDA FLOAT)
 
 AS 
 BEGIN
  -----------------------------------------------------
  --Variables 
  -----------------------------------------------------
DECLARE @V_REFERENCIA NVARCHAR(18)
DECLARE @V_DESCRIPCION NVARCHAR(60)
DECLARE @V_SERIDOC NVARCHAR(4)
DECLARE @V_NUMERODOC INT
DECLARE @V_FECHADOC DATE
DECLARE @V_CANTIDAD FLOAT
DECLARE @V_COSTO FLOAT
DECLARE @V_ACUMULADOCOMPRAS FLOAT
DECLARE @V_FACTORMONEDA FLOAT

-------------------------------------------------------
--Retornamos el valor de la funcion en formato tabla
------------------------------------------------------

DECLARE @TABLA_COMPRAS TABLE
 (id INT IDENTITY(1,1),
 REFERENCIA NVARCHAR(18),
 DESCRIPCION NVARCHAR(60),
 SERIDOC NVARCHAR(4),
 NUMERODOC INT,
 FECHADOC DATE,
 CANTIDAD FLOAT,
 COSTO FLOAT)

 DECLARE @totalregisto INT
 DECLARE @icontador INT
 SET @icontador = 1
 SET @V_ACUMULADOCOMPRAS = 0
 ---------------------------------------------------------
 --Llenar la tabla temporal
 ---------------------------------------------------------

INSERT INTO @TABLA_COMPRAS
SELECT  ALBLIN.REFERENCIA, ALBLIN.DESCRIPCION, ALBCAB.NUMSERIE, ALBCAB.NUMALBARAN,  ALBCAB.FECHAALBARAN, ALBLIN.UNIDADESTOTAL, ALBLIN.PRECIO
 FROM   ALBCOMPRACAB AS ALBCAB WITH (NOLOCK)
INNER JOIN 
ALBCOMPRALIN AS ALBLIN WITH (NOLOCK) ON ALBLIN.NUMSERIE =ALBCAB.NUMSERIE AND ALBCAB.NUMALBARAN = ALBLIN.NUMALBARAN AND ALBCAB.N = ALBLIN.N
WHERE  ALBCAB.FECHAALBARAN BETWEEN @P_FECHAINIT AND @P_FECHAFIN 

----------------
--Iniciar Ciclo
----------------

SELECT @totalregisto = count(*)
FROM @TABLA_COMPRAS

WHILE @icontador <= @totalregisto
BEGIN

SELECT @V_REFERENCIA=REFERENCIA,
       @V_DESCRIPCION=DESCRIPCION,     
	   @V_NUMERODOC = NUMERODOC,
	   @V_FECHADOC = FECHADOC,
	   @V_CANTIDAD = CANTIDAD,
	   @V_COSTO = COSTO
FROM @TABLA_COMPRAS
WHERE id = @icontador

	
	---------------------------------------------------------
	--Se acumula la cantidad de unidades compradas
    ---------------------------------------------------------
	SET @V_ACUMULADOCOMPRAS = @V_ACUMULADOCOMPRAS + ISNULL(@V_CANTIDAD,0)

	SET @V_FACTORMONEDA =1

	EXEC @V_FACTORMONEDA = [dbo].[F_GET_COTIZACION] @V_FECHADOC, 2



	INSERT INTO @TBL_COMPRASARTICULOS (REFERENCIA,DESCRIPCION,SERIDOC,NUMERODOC,FECHADOC,CANTIDAD,ACUMULADOCOMPRAS,FACTORMONEDA)
	VALUES (@V_REFERENCIA,@V_DESCRIPCION,@V_SERIDOC,@V_NUMERODOC,@V_FECHADOC,@V_CANTIDAD,@V_ACUMULADOCOMPRAS,@V_FACTORMONEDA)
	
	---------------------------------------------------------
	-- Se van recorriendo las tablas
	----------------------------------------------------------
	SET @icontador = @icontador + 1  
END

RETURN

END
GO


